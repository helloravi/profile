def countlet(a,b)
  i = 0
  j = 0
  while(i<a.length)
    while(j<b.length)
      if(a[i]==b[j])
        a[i]=""
        b[j]=""
      end
      j=j+1
    end
    i=i+1
    j=0
  end
  return (a+b).length
end

def answer(num, c)
  if(c.length==1)
    return c[0]
  elsif (num%c.length == 0)
    # return answer(num, c[0..c.length-2])
    return answer(num, c[0..-2])
  else
    # return answer(num, c[num%c.length..c.length-1]+c[0..num%c.length-2])
    return answer(num, c[num%c.length..-1]+c[0..num%c.length-2])
  end

end

def flames(a,b,c)
  num = countlet(a,b)
  h = {"f"=>"friends", "l"=>"lovers", "a"=>"affair", "m"=>"marriage", "e"=>"enemies", "s"=>"siblings"}
  return h[answer(num, c)]
end

puts flames("rohit","rajan", "flames")
# puts countlet("rohit","rajan")