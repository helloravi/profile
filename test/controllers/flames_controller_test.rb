require 'test_helper'

class FlamesControllerTest < ActionController::TestCase
  test "should get jindex" do
    get :jindex
    assert_response :success
  end

  test "should get rindex" do
    get :rindex
    assert_response :success
  end

end
