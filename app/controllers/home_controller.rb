class HomeController < ApplicationController
  def index
  end

  def portfolio
  end

  def about
  end

  def blog
  end

  def puzzle
  end

  def astra
    redirect_to "https://docs.google.com/spreadsheets/d/1YoDLnlBxtfyaL-YU1-lkfsMXLDfiXRiWg_9_N5n4xqQ/edit#gid=0"
  end
  
end
