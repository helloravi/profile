module HomeHelper
  # def nav_items
  #   nav_item("Home", root_path, action_name == "index") +
  #     nav_item("Portfolio", home_portfolio_path, action_name == "portfolio") +
  #     nav_item("About", home_about_path, action_name == "about") +
  #     nav_item("Blog", home_blog_path, action_name == "blog") +
  #     nav_item("Puzzles", home_puzzle_path, action_name == "puzzle") :blog, :puzzle,
  # end

  def nav_items
    [:index, :portfolio, :about, :new].collect do |actn|
      nav_item(nav_link_text(actn), nav_link_path(actn), action_name.to_sym == actn)
    end.reduce(&:+)
  end

  def nav_link_text(actn)
    {index: "Home", portfolio: "Portfolio", about: "About", blog: "Blog", puzzle: "Puzzles",
     new: "Contact"}[actn]
  end

  def nav_link_path(actn)
    {index: root_path, portfolio: home_portfolio_path, about: home_about_path, blog: home_blog_path,
     puzzle: home_puzzle_path, new: new_contact_path}[actn]
  end

  def nav_item(txt, link, active)
    content_tag(:li, class: (active ? "active" : "")) do
      link_to txt, link
    end
  end
end
